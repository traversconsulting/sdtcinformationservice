﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerInformationSerivce
{
    class Drive
    {
        public String drive;
        public String type;
        public String filesystemType;
        public bool isReady;
        public long userFreeSpace;
        public long totalFreeSpace;
        public long totalSize;
    }
}
