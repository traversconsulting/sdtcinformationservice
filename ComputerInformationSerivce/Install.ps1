﻿$SD_DIR=$Env:Programfiles+"\ServiceDesk"
$SERVICE_DIR=$SD_DIR+"\Service"
If(!(test-path $SD_DIR))
{
      New-Item -ItemType Directory -Force -Path $SD_DIR
}
If(!(test-path $SERVICE_DIR))
{
      New-Item -ItemType Directory -Force -Path $SERVICE_DIR
}
else{
	echo TicketService folder already exists.
    echo Deleting Existing TicketService folder...
    Remove-Item -Recurse $SERVICE_DIR
    echo Done.
}
robocopy ".\bin\Release" $SERVICE_DIR"\Release" /log:$SD_DIR"\log.txt"
New-Service -Name "ComputerInformationService" -BinaryPathName $SERVICE_DIR"\Release\ComputerInformationService.exe"