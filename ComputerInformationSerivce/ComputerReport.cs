﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputerInformationSerivce
{
    class ComputerReport
    {
        public DateTime createdAt;
        public double processorUsagePercent;
        public String userName;
        public Process activeProcess;
        public List<Drive> drives;
        public String hostName;
        public List<IpAddress> ipAddresses;
        public double ramUsage;
    }
}
