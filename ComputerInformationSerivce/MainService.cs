﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Management;
using System.IO.Pipes;
using System.Timers;
using System.Configuration;
using System.Net.Http;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ComputerInformationSerivce
{
    public partial class MainService : ServiceBase
    {
        private Thread workThread;
        private bool takeScreenshot = false;
        private string processResults = "";
        private string name;
        private NamedPipeServerStream userPipe;

        private void EndWaitEvent(Object source, ElapsedEventArgs e)
        {
            //If we aren't in the state of connected
            if (!userPipe.IsConnected)
            {
                NamedPipeClientStream userClientPipe = new NamedPipeClientStream(".", name + "-pipe", PipeDirection.InOut);
                //Connect to the server pipe (Ran on the service) this blocks until it makes a successful connection
                userClientPipe.Connect();
                userClientPipe.ReadMode = PipeTransmissionMode.Message;
                StringBuilder messageBuilder = new StringBuilder();
                //Receive the initial message which is going to be directions on whether or not to take a screenshot
                string messageChunk = string.Empty;
                byte[] messageBuffer = new byte[5];
                do
                {
                    userClientPipe.Read(messageBuffer, 0, messageBuffer.Length);
                    messageChunk = Encoding.UTF8.GetString(messageBuffer);
                    messageBuilder.Append(messageChunk);
                    messageBuffer = new byte[messageBuffer.Length];
                }
                while (!userClientPipe.IsMessageComplete);
                string response = "\"Unable to get information from the user's session, the agent application may be off\"";
                byte[] messageBytes = Encoding.UTF8.GetBytes(response);
                //Write to the pipe
                userClientPipe.Write(messageBytes, 0, messageBytes.Length);
                userClientPipe.Close();
            }
        }
        public MainService()
        {
            InitializeComponent();
        }
        public void OnDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {
            workThread = new Thread(ProcessingLoop);
            workThread.Start();
        }

        private async void SendComputerInformation(object sender, System.Timers.ElapsedEventArgs e)
        {
            ComputerReport computerReport = new ComputerReport();
            computerReport.hostName = Dns.GetHostName();
            computerReport.ipAddresses = getIpAddresses();
            computerReport.ramUsage = getRamPercentage();
            computerReport.userName = getUserName();
            computerReport.drives = getDrives();
            computerReport.activeProcess = getActiveProcess();
            computerReport.createdAt =DateTime.UtcNow;
            computerReport.processorUsagePercent = getProcessorUsage();
            // Construct a json to send to the server.
            string jsonString = JsonConvert.SerializeObject(computerReport);
            HttpClient client = new HttpClient();
            HttpContent content = new StringContent(jsonString, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(ConfigurationManager.AppSettings["ServerURL"]+"/api/ComputerReport/", content).Result;
            String responseBody = await response.Content.ReadAsStringAsync();
        }

        private void ProcessingLoop()
        {
            //Do this every hour
            System.Timers.Timer timer = new System.Timers.Timer(Convert.ToInt64(ConfigurationManager.AppSettings["RuntimeFrequency"]));
            timer.Elapsed += SendComputerInformation;
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            workThread.Abort();
        }

        private double getProcessorUsage()
        {
            //Get CPU usage values using a WMI query
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_PerfFormattedData_PerfOS_Processor");
            var cpuTimes = searcher.Get();
            foreach(ManagementObject processorObject in searcher.Get())
            {
                if(processorObject["Name"].Equals("_Total"))
                {
                    return Double.Parse(processorObject["PercentProcessorTime"].ToString());
                }
            }

            return 0;
        }

        private String getUserName()
        {
            //Get domain and username in the DOMAIN\NAME format, this is accomplished by getting the explorer.exe process and seeing what user is running this process
            ManagementObjectSearcher Processes = new ManagementObjectSearcher("Select * from Win32_Process");
            foreach (System.Management.ManagementObject Process in Processes.Get())
            {
                if (Process["ExecutablePath"] != null &&
                    System.IO.Path.GetFileName(Process["ExecutablePath"].ToString()).ToLower() == "explorer.exe")
                {
                    string[] OwnerInfo = new string[2];
                    Process.InvokeMethod("GetOwner", (object[])OwnerInfo);
                    return OwnerInfo[0];
                }
            }
            return "";
        }

        private Process getActiveProcess()
        {
            try
            {
                //Get domain and username in the DOMAIN\NAME format, this is accomplished by getting the explorer.exe process and seeing what user is running this process
                ManagementObjectSearcher Processes = new ManagementObjectSearcher("Select * from Win32_Process");
                foreach (System.Management.ManagementObject Process in Processes.Get())
                {
                    if (Process["ExecutablePath"] != null &&
                        System.IO.Path.GetFileName(Process["ExecutablePath"].ToString()).ToLower() == "explorer.exe")
                    {
                        string[] OwnerInfo = new string[2];
                        Process.InvokeMethod("GetOwner", (object[])OwnerInfo);
                        name = OwnerInfo[1] + "\\" + OwnerInfo[0];
                        break;
                    }
                }
                PipeSecurity pipeSecurity = new PipeSecurity();
                PipeAccessRule psEveryone = new PipeAccessRule("Everyone", PipeAccessRights.FullControl, System.Security.AccessControl.AccessControlType.Allow);
                pipeSecurity.AddAccessRule(psEveryone);
                userPipe = new NamedPipeServerStream(name + "-pipe", PipeDirection.InOut,
        1, PipeTransmissionMode.Message, PipeOptions.None, 1024, 1024, pipeSecurity);
                System.Timers.Timer timer1 = new System.Timers.Timer(2000);
                timer1.Elapsed += EndWaitEvent;
                timer1.Enabled = true;
                //Blocks until client connects
                userPipe.WaitForConnection();
                byte[] messageBytes = Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["TakeScreenshot"]);
                //Write to the pipe
                userPipe.Write(messageBytes, 0, messageBytes.Length);
                StringBuilder messageBuilder = new StringBuilder();
                //A receive loop to get the version info from the user
                string messageChunk = string.Empty;
                byte[] messageBuffer = new byte[5];
                do
                {
                    userPipe.Read(messageBuffer, 0, messageBuffer.Length);
                    messageChunk = Encoding.UTF8.GetString(messageBuffer);
                    messageBuilder.Append(messageChunk);
                    messageBuffer = new byte[messageBuffer.Length];
                }
                while (!userPipe.IsMessageComplete);
                //Close the pipe (let's us create it again on next run if its the same user)
                userPipe.Close();
                processResults = messageBuilder.ToString();
                if (processResults.Equals("\"Unable to get information from the user's session, the agent application may be off\""))
                {
                    return null;
                }
                //{\"title\" : \"SDTCComputerInformationService (Running) - Microsoft Visual Studio \", \"VersionInfo\" : \"File:             C:\\\\Program Files (x86)\\\\Microsoft Visual Studio\\\\2017\\\\Community\\\\Common7\\\\IDE\\\\devenv.exe\\nInternalName:     devenv.exe\\nOriginalFilename: devenv.exe\\nFileVersion:      15.9.28307.960 built by: D15.9\\nFileDescription:  Microsoft Visual Studio 2017\\nProduct:          Microsoft® Visual Studio®\\nProductVersion:   15.9.28307.960\\nDebug:            False\\nPatched:          False\\nPreRelease:       False\\nPrivateBuild:     False\\nSpecialBuild:     False\\nLanguage:         English (United States)\\n\"}
                dynamic d = JObject.Parse(processResults);
                Process activeProcess = new Process();
                activeProcess.title = d.title;
                activeProcess.versionInfo = d.VersionInfo;
                return activeProcess;
            }
            catch(Exception e)
            {
                return null;
            }
        }

        private List<Drive> getDrives()
        {
            List<Drive> outDrives = new List<Drive>();
            //create a string with each drive
            DriveInfo[] drives = DriveInfo.GetDrives();
            StringBuilder driveSB = new StringBuilder("[");
            foreach (DriveInfo drive in drives)
            {
                Drive tempDrive = new Drive();
                tempDrive.drive = drive.Name.Replace(@"\", @"\\");
                tempDrive.type = drive.DriveType.ToString();
                tempDrive.isReady = drive.IsReady;
                if (tempDrive.isReady)
                {
                    tempDrive.filesystemType = drive.DriveFormat;
                    tempDrive.userFreeSpace = drive.AvailableFreeSpace;
                    tempDrive.totalFreeSpace = drive.TotalFreeSpace;
                    tempDrive.totalSize = drive.TotalSize;
                }
                outDrives.Add(tempDrive);
            }
            return outDrives;
        }

        private List<IpAddress> getIpAddresses()
        {
            string hostname = Dns.GetHostName();
            List<IpAddress> ips = new List<IpAddress>();
            IPAddress[] addresses = Dns.GetHostEntry(hostname).AddressList;
            foreach (IPAddress address in addresses)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    IpAddress ipAddress = new IpAddress();
                    ipAddress.ipAddress = address.ToString();
                    ips.Add(ipAddress);
                }
            }
            return ips;
        }

        private double getRamPercentage()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "Select * from Win32_OperatingSystem");
            double ramPercentage = 0;
            foreach (ManagementObject queryObj in searcher.Get())
            {
                double free = Double.Parse(queryObj["FreePhysicalMemory"].ToString());
                double total = Double.Parse(queryObj["TotalVisibleMemorySize"].ToString());
                ramPercentage = Math.Round((total - free) / total * 100, 2);
            }
            return ramPercentage;
        }
    }
}
